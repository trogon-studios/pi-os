format binary as 'img'

start:
   b main

include 'gpio.asm'
include 'timer.asm'

main:

   mov r0, #16	    ;mov pinNum, #16
   mov r1, #1	    ;mov pinFunc, #1
   bl SetGpioFunction

   ;ptrn .req r4
   ldr r4, [pattern]	 ;ldr ptrn, =pattern
   ;ldr r4, [r4]          ;ldr ptrn, [ptrn]
   ;seq .req r5
   mov r5, #0		;mov seq, #0

   for1:
      mov r2, #1
      lsl r2, r5	   ;lsl r1, seq
      and r2, r4	   ;and r1, ptrn

      mov r0, #16     ;mov pinNum, #16
      mov r1, r2      ;mov pinVal, #0
      bl SetGpio

      mov r0, #0x40000
      bl TimerWait

      add r5, #1
      cmp r5, #32
      movhi r5, #0

      b for1

hang:
; jump to label "hang"
   b hang


;.section .data
;.align 2
pattern:
dd 0x1FF544455 ;.int 0b111111111010101000100010001010101