; Get and save GIPO Address into r0
GetGpioAddress:
   ldr r0, [GPFSEL0]
   mov pc, lr ;Return

; Set GIPO pin function
SetGpioFunction:
   cmp r0, #53 ; r0 == 53
   cmpls r1, #7 ; r1 == 7 if r0 <= 7
   movhi pc, lr ; Return if r1 > 7

   push {lr}
   mov r2, r0 ; r2 = r0
   bl GetGpioAddress

   functionLoop$:
      cmp r2, #9 ; r2 == 9 r2 - numer pinu
      subhi r2, #10 ; r2 =- 10
      addhi r0, #4 ; r0 =+ 4
      bhi functionLoop$

   add r2, r2, lsl #1 ; r2 =* 3
   lsl r1, r2 ; r1 = r1 << r2
   str r1, [r0]
   pop {pc} ; Return

; Set GIPO pin value
SetGpio:
   ;pinNum .req r0
   ;pinVal .req r1

   cmp r0, #53          ;cmp pinNum, #53
   movhi pc, lr         ; Return if r0 > 53
   push {lr}
   mov r2, r0           ;mov r2, pinNum
   ;.unreq pinNum
   ;pinNum .req r2
   bl GetGpioAddress
   ;gpioAddr .req r0

   ;pinBank .req r3
   ;lsr r3, r2, #5       ;lsr pinBank, pinNum, #5
   ;lsl r3, #2           ;lsl pinBank, #2
   ;add r0, r3           ;add gpioAddr, pinBank
   ;and r2, #31          ;and pinNum, #31
   ;.unreq pinBank
   
   cmp r2, #31
   addhi r0, #4
   movhi r2, #1

   ;setBit .req r3
   mov r3, #1           ;mov setBit, #1
   lsl r3, r2           ;lsl setBit, pinNum
   ;.unreq pinNum

   teq r1, #0           ;teq pinVal, #0
   ;.unreq pinVal
   streq r3, [r0, #40]  ;streq setBit, [gpioAddr, #40]
   strne r3, [r0, #28]  ;strne setBit, [gpioAddr, #28]
   ;.unreq setBit
   ;.unreq gpioAddr
   pop {pc}

GPFSEL0: dw 0x20200000