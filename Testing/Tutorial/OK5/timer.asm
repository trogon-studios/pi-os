; Get and save Counter Address into r0
GetTimerAddress:
   ldr r0, [TIMERSEL0]
   mov pc, lr ;Return  

; Get current counter into (r1,r0)
GetTimeStamp:
   push {lr}
   bl GetTimerAddress
   ldrd r0, r1, [r0, #4]
   pop {pc}

; Wait r0 micro seconds
TimerWait:
   ;delay .req r2
   mov r2, r0		;mov delay, r0  
   push {lr}
   bl GetTimeStamp
   ;start .req r3
   mov r3, r0		;mov start, r0

   loop$:
      bl GetTimeStamp
      ;elapsed .req r1
      sub r1, r0, r3	;sub elapsed, r0, start
      cmp r1, r2	;cmp elapsed, delay
      ;.unreq elapsed
      bls loop$
	
   ;.unreq delay
   ;.unreq start
   pop {pc}

TIMERSEL0: dw 0x20003000