format binary as 'img'

start:
   b main

include 'gpio.asm'

;.section '.text'
main:
   ;mov sp, #0x8000

   ;pinNum .req r0
   ;pinFunc .req r1
   mov r0, #16	    ;mov pinNum, #16
   mov r1, #1	    ;mov pinFunc, #1
   bl SetGpioFunction
;   .unreq pinNum
;   .unreq pinFunc

;   pinNum .req r0
;   pinVal .req r1
   mov r0, #16	   ;mov pinNum, #16
   mov r1, #0	   ;mov pinVal, #0
   bl SetGpio
;   .unreq pinNum
;   .unreq pinVal

hang:
; jump to label "hang"
   b hang

