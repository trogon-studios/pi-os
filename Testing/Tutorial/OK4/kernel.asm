format binary as 'img'

start:
   b main

include 'gpio.asm'
include 'timer.asm'

main:
   mov r0, #16	    ;mov pinNum, #16
   mov r1, #1	    ;mov pinFunc, #1
   bl SetGpioFunction

   ledLoop:
      mov r0, #16	   ;mov pinNum, #16
      mov r1, #0	   ;mov pinVal, #0
      bl SetGpio

      mov r0, #0x1F0000
      bl TimerWait

      mov r0, #16	   ;mov pinNum, #16
      mov r1, #1	   ;mov pinVal, #0
      bl SetGpio

      mov r0, #0x1F0000
      bl TimerWait

      b ledLoop

hang:
; jump to label "hang"
   b hang
