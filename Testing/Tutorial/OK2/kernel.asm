format binary as 'img'

start:
;
; This command loads the physical address of the GPIO region into r0.
;
   ldr r0, [GPFSEL0]

;
; Our register use is as follows:
; r0=0x20200000 the address of the GPIO region.
; r1=0x00040000 a number with bits 18-20 set to 001 to put into the GPIO
;                               function select to enable output to GPIO 16. 
; then
; r1=0x00010000 a number with bit 16 high, so we can communicate with GPIO 16.
;
   mov r1, #1         ;Output mode
   lsl r1, #18       ;GPIO16

;
; Set the GPIO function select.
;
   str r1, [r0, #4]   ;GPFSEL1
; 
; Set the 16th bit of r1.
;
   mov r1, #1        ;Clear
   lsl r1, #16        ;GPIO16
; 
; Set GPIO 16 to low, causing the LED to turn on.
;
   str r1, [r0, #40]   ;GPCLR0

   mov r2, #0x3F0000
   wait1:
      sub r2, #1
      cmp r2, #0
      bne wait1

   str r1, [r0, #28]

hang:
   b hang

GPFSEL0: dw 0x20200000